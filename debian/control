Source: nanofilt
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Shayan Doust <hello@shayandoust.me>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               python3-all,
               python3-setuptools,
               dh-python,
               python3-biopython,
               python3-pandas,
               python3-flake8
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/nanofilt
Vcs-Git: https://salsa.debian.org/med-team/nanofilt.git
Homepage: https://github.com/wdecoster/nanofilt
Rules-Requires-Root: no

Package: nanofilt
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-biopython,
         python3-pandas
Description: filtering and trimming of long read sequencing data
 Filtering and trimming of long read sequencing data. Filtering on
 quality and/or read length, and optional trimming after passing filters.
 Reads from stdin, writes to stdout. Optionally reads directly from an
 uncompressed file specified on the command line.
 .
 Intended to be used:
 .
  1. directly after fastq extraction.
  2. prior to mapping.
  3. in a stream between extraction and mapping.
